const { RTMClient } = require('@slack/client')
const axios = require('axios')
require('dotenv').config()

const axiosToken = process.env.AXIOS_TOKEN

const rtm = new RTMClient(process.env.TOKEN)

rtm.start()

rtm.on('reaction_added', event => {
    if(event.reaction === 'lmgtfy') {
        axios.get(`https://slack.com/api/channels.history?token=${axiosToken}&channel=${event.item.channel}&latest=${event.item.ts}&inclusive=true&count=1`)
        .then(res => {
            rtm.sendMessage(`http://lmgtfy.com/?q=${res.data.messages[0].text.split(' ').join('+')}`, event.item.channel)
        })
    }
})
